package com.team1.forest

import com.team1.forest.settings.Settings
import com.team1.forest.trees.Tree

class Forest(val width: Int, val height: Int) {

    val trees: List<List<Tree?>> = List(width) { i ->
        List(height) { j ->
            if (Settings.genTreeOrNot())
                Settings.genTree(i to j)
            else
                null
        }
    }

    /**
     * Возвращает соседей по указаным координатам
     */
    fun getNeighbors(coords: Pair<Int, Int>) =
        (-1..1)
            .flatMap { x -> (-1..1).map { y -> x to y } } // pairs like -1,-1 -1,0 ... 1,1
            .filter { it != 0 to 0 } // drop center
            .map { (x, y) -> coords.first + x to coords.second + y }// add to coords
            .filter { (x, y) -> x in 0 until width && y in 0 until height } // stay inside the boundaries
            .mapNotNull { (x, y) -> trees[x][y] } // filter not null trees

    init {
    }
}
package com.team1.forest

import com.team1.forest.animals.Animal
import com.team1.forest.settings.Settings

interface Updatable {
    /** Starting from 1 */
    fun update(tick: Int)
    
//    fun reset()
}


class TreeController(
    val forest: Forest
) : Updatable {
    
    override fun update(tick: Int) {
        
        println("--------------------------------- DAY $tick")
        
        if (tick % Settings.foodUpdateFrequency == 0) {
            updateFood()
        }
        printTrees()
    }

    private fun printTrees() {
        forest.trees
            .flatten()
            .forEach { tree ->
                println(tree)
            }
    }

    fun updateFood() {
        forest.trees
            .flatten()
            .filterNotNull()
            .forEach { tree ->
                tree.allFood.forEach { food ->
                    food.updateCount()
                }
        }
    }
}

class AnimalController(
    val population: Population
): Updatable {

    override fun update(tick: Int) {
        val deadAnimals: MutableList<Animal> = mutableListOf()
        val newBornAnimals: MutableList<Animal> = mutableListOf()

        population.animals
            .forEach { animal ->
                val status = animal.eatMoveMakeChildren()

                if(status == 4)
                    newBornAnimals.addAll(Settings.genNewAnimals(animal))

                val animalWhichMightBeDead = animal.checkHealth()

                if (animalWhichMightBeDead != null){
                    deadAnimals.add(animalWhichMightBeDead)
                }
            }

        deadAnimals.forEach { population.animals.remove(it) }
        population.animals.addAll(newBornAnimals)

        printAnimals()
    }

    private fun printAnimals(){
        print("size=${population.animals.size}   ")
        population.animals
            .forEach { print("Animal(${it.name} id=${it.id} ${it.health})") }
        println()
    }
}

/**
 * change epochNumber to get more ticks
 */
class Timer(
    val updatables: List<Updatable>,
    val epochNumber: Int = 10
) {
    
    fun start() {
        for (i in 1..epochNumber) {
            updatables.forEach { it.update(i) }
        }
    }
    
}

object EcoCoordinator {
    val forest = Forest(Settings.forestWidth, Settings.forestHeight)

    var animals = Population(forest)

    fun start() {
        Timer(listOf(TreeController(forest), AnimalController(animals))).start()
    }
}

fun main(args: Array<String>) {
    println("Hello World")
    EcoCoordinator.start()
}
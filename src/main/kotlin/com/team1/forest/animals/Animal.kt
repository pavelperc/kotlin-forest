package com.team1.forest.animals

import com.team1.forest.EcoCoordinator
import com.team1.forest.food.FoodType
import com.team1.forest.settings.Settings
import com.team1.forest.trees.Tree
import com.team1.forest.trees.home.Home
import com.team1.forest.trees.home.HomeType
import com.team1.forest.trees.leveltree.LevelEnum
import com.team1.forest.trees.leveltree.LevelTree
import kotlin.random.Random

/**
 * Абстрактный класс для животных
 * у животного есть определённый набор действий, которое каждое животное реализует по своему:
 *      передвижение по дереву (уровням)
 *      вступить с другим животным в контакт (размножение и хищнечесвто)
 *      передвижение на другое дерево и его уровень там
 */
abstract class Animal(var tree: Tree) {

    companion object Shared {
        private var id: Int = 0
        fun getId(): Int = id++
    }

    abstract var id: Int
    /**
     * Уровни дерева, где может находиться животное
     */
    abstract var possibleCurrentLevels: List<LevelEnum>

    /**
     * Еда, которую может есть зверь
     */
    abstract var possibleFood: List<FoodType>

    /**
     * Еда, которую может есть зверь
     */
    abstract var possibleHome: List<HomeType>

    /**
     * Здоровье животного
     * Рандомлю для интерактивности
     */
    var health: Int = Random.nextInt(3, Settings.animalHealthMaxBound)

    /**
     * Пол
     */
    val sex: Int = Random.nextInt(2)

    /**
     * Имя
     */
    abstract val name: String

    /**
     * Положение на дереве
     */
    abstract var currentPosition: LevelEnum

    /**
     * Количество еды которую нужно скушать животному чтобы health++
     */
    abstract val amountOfFoodToLive: Int

    /**
     * Статус нахождения дома
     */
    var homeStatus: Home? = null

    /**
     * Изменение здоровья кажый тик?
     */
    fun checkHealth(): Animal? {
        if (health <= 0) {
            when (currentPosition) {
                LevelEnum.crown -> tree.crown.currentAnimals.remove(this)
                LevelEnum.trunk -> tree.trunk.currentAnimals.remove(this)
                LevelEnum.root -> tree.root.currentAnimals.remove(this)
            }

            print("${this.name} is dead and removed from ${currentPosition.name} on ${tree.javaClass.simpleName}\n")
            return this
        }
        return null
    }

    /**
     *  Название возможно не отражает сути: если еда есть, увеличиваем здоровье, кушаем ,
     *  иначе -- делаем что-то другое, активность
     *  возврат значения для реакции леса на действие зверя
     *  1 - переместился на другое дерево
     *  2 - переместился на другой уровень
     *  3 - уснул или спрятался
     *  4 - произвёл потомство
     */
    fun eatMoveMakeChildren(): Int {

        // всё же сначала надо вылезти из дома, проветриться, так сказать
        if (homeStatus != null) {
            // лол, не работает умная проверка языка на не нул((
            homeStatus!!.animals.remove(this)
            homeStatus = null
        }
        // сначала кушаем потом все остальное
        if (health <= Settings.animalHealthMaxBound) {

            if (eatFood()) {
                if (health != Settings.animalHealthMaxBound)
                    health++
            } else {
                health--
            }
        }

        val haveFood = when (currentPosition) {
            LevelEnum.crown -> tree.crown.checkRecources(this)
            LevelEnum.trunk -> tree.trunk.checkRecources(this)
            LevelEnum.root -> tree.root.checkRecources(this)
        }
        val listNeighbors = when (currentPosition) {
            LevelEnum.crown -> tree.crown.getNeighbors(this)
            LevelEnum.trunk -> tree.trunk.getNeighbors(this)
            LevelEnum.root -> tree.root.getNeighbors(this)
        }

        var randToAction = Random.nextInt(7)

        // если рядом хищник, то прячься или убегай
        if (listNeighbors.any { it is Wolf || it is Kite })
            randToAction = Random.nextInt(7)

        // если нет еды на дереве - ищи на другом
        if (!haveFood && health < Settings.animalHealthMaxBound / 2)
            randToAction = Random.nextInt(4)

        when (randToAction) {
            in 0..1 -> if (moveToTheNextTree()) return 1
            in 2..3 -> if (moveWithinCurrentTree()) return 2
            in 4..5 -> if (hideOrSleep()) return 3
            6 -> if (makeChildren()) return 4

            else -> return 0
        }
        return 0
    }

    private fun eatFood(): Boolean {
        return when (currentPosition) {
            LevelEnum.crown -> tree.crown.reduceResources(this)
            LevelEnum.trunk -> tree.trunk.reduceResources(this)
            LevelEnum.root -> tree.root.reduceResources(this)
        }
    }

    /**
     * Метод в каждом классе животных будет реализован по своему в силу способностей передвижения
     * Рандомно будет принято решение о передвижении на какой-либо уровень дерева
     */
    fun moveWithinCurrentTree(): Boolean {

        val randomSize = possibleCurrentLevels.size
        var level = possibleCurrentLevels.get(Random.nextInt(randomSize))
        while (level == currentPosition) {
            level = possibleCurrentLevels.get(Random.nextInt(randomSize))
        }
        print("!!! Moving ${this.name}${this.id} from ${currentPosition.name} to ${level.name} on ${tree.javaClass.simpleName} !!!\n")
        removeFromCurrentLevel(currentPosition)
        addToNewCurrentLevel(level)
        this.currentPosition = level
        return true
    }

    /**
     * Приватная функция для удаления с уровня дерева
     */
    private fun removeFromCurrentLevel(level: LevelEnum) {
        val levelObject: LevelTree
        when (level) {
            LevelEnum.crown -> levelObject = tree.crown
            LevelEnum.trunk -> levelObject = tree.trunk
            LevelEnum.root -> levelObject = tree.root
        }
        levelObject.currentAnimals.remove(this)
    }

    /**
     * Приватная функция посадки на новый уровень
     */
    private fun addToNewCurrentLevel(level: LevelEnum) {
        val levelObject: LevelTree
        when (level) {
            LevelEnum.crown -> levelObject = tree.crown
            LevelEnum.trunk -> levelObject = tree.trunk
            LevelEnum.root -> levelObject = tree.root
        }
        levelObject.currentAnimals.add(this)
    }

    /**
     * Метод в каждом классе животных будет реализован по своему в силу способностей передвижения
     * Рандомно будет принято решение о передвижении на какой-либо уровень соседнего дерева
     */
    fun moveToTheNextTree(): Boolean {
        //val listTrees = EcoCoordinator.forest.getNeighbors(tree.coord).filter { it.allFood.all { it1 -> possibleFood.contains(it1)} }
        // TODO: исправить рандом на осознаный выбор
        val listTrees = EcoCoordinator.forest.getNeighbors(tree.coord)
        if (listTrees.isEmpty())
            return false
        removeFromCurrentLevel(currentPosition)
        val tree4Debug = tree
        tree = listTrees[Random.nextInt(listTrees.size)]
        addToNewCurrentLevel(currentPosition)
        print("!!! Jumping ${this.name}${this.id} from ${tree4Debug.javaClass.simpleName} to ${tree.javaClass.simpleName} !!!\n")
        return true
    }

    fun hideOrSleep(): Boolean {
        val levelObject: LevelTree
        when (currentPosition) {
            LevelEnum.crown -> levelObject = tree.crown
            LevelEnum.trunk -> levelObject = tree.trunk
            LevelEnum.root -> levelObject = tree.root
        }



        // TODO: переписать на меньшее время
        val listHomes: MutableList<Home> = mutableListOf()
        for (i in 0 until levelObject.homes.size)
            for (j in 0 until possibleHome.size)
                if (levelObject.homes[i].equalType(possibleHome[i]))
                    listHomes.add(levelObject.homes[i])



        for (i in 0 until listHomes.size)
            if (listHomes[i].animals.size < Settings.maxAnimalInHome) {
                listHomes[i].animals.add(this)
                homeStatus = listHomes[i]
                print("!!! Hiding ${this.name}${this.id} in ${homeStatus!!.javaClass.simpleName} on ${tree.javaClass.simpleName} !!!\n")
                return true
            }
        return false
    }

    fun makeChildren(): Boolean {
        val levelObject: LevelTree = when (currentPosition) {
            LevelEnum.crown -> tree.crown
            LevelEnum.trunk -> tree.trunk
            LevelEnum.root -> tree.root
        }
        // поиск потенциальных возлюбленных
        val listSweetheart = levelObject.getSweetheart(this)
        if (listSweetheart.isEmpty())
            return false
        // выбор одной-единственной, чтоб на всю жизнь
        val sweetheart = listSweetheart[Random.nextInt(listSweetheart.size)]


        // TODO: переписать на меньшее время
        val listHomes: MutableList<Home> = mutableListOf()
        for (i in 0 until levelObject.homes.size)
            for (j in 0 until possibleHome.size)
                if (levelObject.homes[i].equalType(possibleHome[i]))
                    listHomes.add(levelObject.homes[i])



        for (i in 0 until listHomes.size)
            if (listHomes[i].animals.size < Settings.maxAnimalInHome - 1) {
                listHomes[i].animals.add(this)
                listHomes[i].animals.add(sweetheart)
                homeStatus = listHomes[i]
                sweetheart.homeStatus = listHomes[i]
                print("!!! ${this.name}${this.id} make child with ${sweetheart.name}${sweetheart.id} in ${homeStatus!!.javaClass.simpleName} on ${tree.javaClass.simpleName} !!!\n")
                return true
            }
        return false
    }
}

/**
 * Класс Барсук
 */
class Badger(

    var treecurr: Tree,
    override var possibleCurrentLevels: List<LevelEnum> = listOf(LevelEnum.root, LevelEnum.trunk),
    override var id: Int = getId(),
    override var possibleFood: List<FoodType> = listOf(FoodType.Nut, FoodType.MapleLeaf),
    override var possibleHome: List<HomeType> = listOf(HomeType.Den)

) : Animal(treecurr) {

    override val name: String = this.javaClass.simpleName

    override var currentPosition: LevelEnum = LevelEnum.trunk

    override val amountOfFoodToLive: Int = 10

    /**
     * При инициализации сажаем животное на нужный уровень
     */
    init {
        treecurr.trunk.currentAnimals.add(this)
    }

}

/**
 *  Класс бурундук
 */
class Chipmunk(

    var treecurr: Tree,
    override var possibleCurrentLevels: List<LevelEnum> = listOf(LevelEnum.root, LevelEnum.trunk),
    override var id: Int = getId(),
    override var possibleFood: List<FoodType> = listOf(FoodType.RootCrop, FoodType.Worm, FoodType.Nut),
    override var possibleHome: List<HomeType> = listOf(HomeType.Hollow, HomeType.Den)

) : Animal(treecurr) {

    override val name: String = this.javaClass.simpleName

    override var currentPosition: LevelEnum = LevelEnum.root

    override val amountOfFoodToLive: Int = 8

    /**
     * При инициализации сажаем животное на нужный уровень
     */
    init {
        treecurr.root.currentAnimals.add(this)
    }

}

/**
 * Белка-летяга
 */
class FlyingSquirrel(

    var treecurr: Tree,
    override var possibleCurrentLevels: List<LevelEnum> = listOf(LevelEnum.root, LevelEnum.trunk, LevelEnum.crown),
    override var id: Int = getId(),
    override var possibleFood: List<FoodType> = listOf(FoodType.Nut, FoodType.MapleLeaf),
    override var possibleHome: List<HomeType> = listOf(HomeType.Hollow, HomeType.Den)

) : Animal(treecurr) {

    override val name: String = this.javaClass.simpleName

    override var currentPosition: LevelEnum = LevelEnum.crown

    override val amountOfFoodToLive: Int = 10

    /**
     * При инициализации сажаем животное на нужный уровень
     */
    init {
        treecurr.crown.currentAnimals.add(this)
    }
}

/**
 * Белка:
 *      ест все!(?)
 */
class Squirrel(

    var treecurr: Tree,
    override var possibleCurrentLevels: List<LevelEnum> = listOf(LevelEnum.root, LevelEnum.trunk),
    override var id: Int = getId(),
    override var possibleFood: List<FoodType> = listOf(FoodType.Cone, FoodType.Nut),
    override var possibleHome: List<HomeType> = listOf(HomeType.Hollow, HomeType.Den)

) : Animal(treecurr) {

    override val name: String = this.javaClass.simpleName

    override var currentPosition: LevelEnum = LevelEnum.trunk

    override val amountOfFoodToLive: Int = 9

    /**
     * При инициализации сажаем животное на нужный уровень
     */
    init {
        treecurr.trunk.currentAnimals.add(this)
    }
}

/**
 * Дятел
 */
class Woodpecker(

    var treecurr: Tree,
    override var possibleCurrentLevels: List<LevelEnum> = listOf(LevelEnum.trunk, LevelEnum.crown),
    override var id: Int = getId(),
    override var possibleFood: List<FoodType> = listOf(FoodType.Worm),
    override var possibleHome: List<HomeType> = listOf(HomeType.Hollow, HomeType.Nest)

) : Animal(treecurr) {

    override val name: String = this.javaClass.simpleName

    override var currentPosition: LevelEnum = LevelEnum.crown

    override val amountOfFoodToLive: Int = 10

    /**
     * При инициализации сажаем животное на нужный уровень
     */
    init {
        treecurr.crown.currentAnimals.add(this)
    }
}

/**
 * Волк
 */
class Wolf(

    var treecurr: Tree,
    override var possibleCurrentLevels: List<LevelEnum> = listOf(LevelEnum.root),
    override var id: Int = getId(),
    override var possibleFood: List<FoodType> = listOf(/*FoodType.Animal*/),
    override var possibleHome: List<HomeType> = listOf(HomeType.Den)

) : Animal(treecurr) {

    override val name: String = this.javaClass.simpleName

    override var currentPosition: LevelEnum = LevelEnum.root

    override val amountOfFoodToLive: Int = 3

    /**
     * При инициализации сажаем животное на нужный уровень
     */
    init {
        treecurr.root.currentAnimals.add(this)
    }
}

/**
 * Коршун
 */
class Kite(

    var treecurr: Tree,
    override var possibleCurrentLevels: List<LevelEnum> = listOf(LevelEnum.crown, LevelEnum.trunk),
    override var id: Int = getId(),
    override var possibleFood: List<FoodType> = listOf(/*FoodType.Animal,*/ FoodType.Worm),
    override var possibleHome: List<HomeType> = listOf(HomeType.Nest)

) : Animal(treecurr) {

    override val name: String = this.javaClass.simpleName

    override var currentPosition: LevelEnum = LevelEnum.crown

    override val amountOfFoodToLive: Int = 1

    /**
     * При инициализации сажаем животное на нужный уровень
     */
    init {
        treecurr.crown.currentAnimals.add(this)
    }
}
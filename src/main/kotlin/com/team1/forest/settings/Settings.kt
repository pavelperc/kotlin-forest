package com.team1.forest.settings

import com.team1.forest.Forest
import com.team1.forest.animals.*
import com.team1.forest.food.FoodType
import com.team1.forest.food.PlantFood
import com.team1.forest.trees.*
import kotlin.random.Random


object Settings {

    const val forestHeight = 1
    const val forestWidth = 3
    var animalNumberInForest = 10
    var animalHealthMaxBound = 6
    const val maxAnimalInHome = 2

    fun genTreeOrNot() = Random.nextDouble() > 0.3

    val hollowNumber: Int
        get() = Random.nextInt(0, 2)

    val nestNumber: Int
        get() = Random.nextInt(0, 2)

    val denNumber: Int
        get() = Random.nextInt(0, 2)

    const val foodUpdateFrequency = 4

    /**
     * Генерация еды:
     *      можно задать тип и спелость */
    fun genFood(type: FoodType, ripenessInTreePart: Int = 1) = when (type) {
        FoodType.Cone -> PlantFood(type, Random.nextInt(6, 16), ripeness = ripenessInTreePart)
        FoodType.MapleLeaf -> PlantFood(type, Random.nextInt(50, 100), maxRipeness = 10)
        FoodType.Nut -> PlantFood(type, Random.nextInt(5, 10), ripeness = ripenessInTreePart)
        FoodType.RootCrop -> PlantFood(type, Random.nextInt(4, 10))
        FoodType.Worm -> PlantFood(type, Random.nextInt(6, 20), maxRipeness = 100000)
    }


    fun genTree(coord: Pair<Int, Int>): Tree = when (Random.nextInt(6)) {
        0 -> Birch(coord)
        1 -> Fir(coord)
        2 -> Maple(coord)
        3 -> Oak(coord)
        4 -> Pine(coord)
        5 -> Walnut(coord)
        else -> throw NotImplementedError("(")
    }

    fun genAnimals(forest: Forest): MutableList<Animal> {

        val resultList: MutableList<Animal> = mutableListOf()

        for (l in 1..Settings.animalNumberInForest) {
            val animal: Animal
            var i = Random.nextInt(Settings.forestWidth)
            var j = Random.nextInt(Settings.forestHeight)

            while (forest.trees[i][j] == null) {
                i = Random.nextInt(Settings.forestWidth)
                j = Random.nextInt(Settings.forestHeight)
            }

            val treeCurrent = forest.trees[i][j]!!
            animal = setAnimalOnTree(treeCurrent)
            resultList.add(animal)
        }

        return resultList
    }

    fun genNewAnimals(parent: Animal, count: Int = 1): MutableList<Animal> {
        val resultList: MutableList<Animal> = mutableListOf()

        for (l in 1..count) {
            val animal: Animal = when (parent) {
                is Badger -> Badger(parent.tree)
                is Chipmunk -> Chipmunk(parent.tree)
                is FlyingSquirrel -> FlyingSquirrel(parent.tree)
                is Squirrel -> Squirrel(parent.tree)
                is Woodpecker -> Woodpecker(parent.tree)
                is Wolf -> Wolf(parent.tree)
                is Kite -> Kite(parent.tree)
                else -> throw NotImplementedError("Animal not existing exception")
            }
            resultList.add(animal)
        }

        return resultList
    }

    fun setAnimalOnTree(treeCurrent: Tree): Animal = when (Random.nextInt(5)) {
        0 -> Badger(treeCurrent)
        1 -> Chipmunk(treeCurrent)
        2 -> FlyingSquirrel(treeCurrent)
        3 -> Squirrel(treeCurrent)
        4 -> Woodpecker(treeCurrent)
        else -> throw NotImplementedError("Animal not existing exception")
    }
}
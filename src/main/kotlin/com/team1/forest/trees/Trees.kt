package com.team1.forest.trees

import com.team1.forest.food.FoodType
import com.team1.forest.food.PlantFood
import com.team1.forest.settings.Settings
import com.team1.forest.trees.leveltree.Crown
import com.team1.forest.trees.leveltree.Root
import com.team1.forest.trees.leveltree.Trunk

/**
 * Типы деревьев
 */

//enum class TypeTree
//{
//    Birch, // береза
//    Fir, // ель
//    Maple, // клён
//    Oak, // дуб
//    Pine, // сосна
//    Walnut, // орех
//
//}


/**
 * Класс Дерево
 */
abstract class Tree(val coord: Pair<Int, Int>) {


    /** Экземпляр кроны*/
    abstract val crown: Crown

    /** Экземпляр ствола*/
    abstract val trunk: Trunk

    /** Экземпляр корня*/
    abstract val root: Root

    val allFood: List<PlantFood>
        get() = crown.food + trunk.food + root.food

    override fun toString() = "Tree(class=${this::class.simpleName},\n\tcoord=$coord,\n\tcrown=$crown," +
            "\n\ttrunk=$trunk,\n\troot=$root)"
}

/** Береза
 *  Еда: крона -
 *       ствол - червяки
 *       корни - корнеплоды и червяки
 * */
class Birch(coord: Pair<Int, Int> = 0 to 0) : Tree(coord) {

    override val crown = Crown()
    override val trunk = Trunk(
        listOf (
            Settings.genFood(FoodType.Worm)
        )
    )

    override val root = Root(
        listOf(
            Settings.genFood(FoodType.RootCrop),
            Settings.genFood(FoodType.Worm)
        )
    )
}

/** Ель
 *  Еда: крона - орехи и шишки
 *       ствол - червяки
 *       корни - опавшие орехи, корнеплоды и опавшие шишки
 * */
class Fir(coord: Pair<Int, Int> = 0 to 0) : Tree(coord) {
    override val crown = Crown(
        listOf(
            Settings.genFood(FoodType.Nut),
            Settings.genFood(FoodType.Cone)
        )
    )
    override val trunk = Trunk(
        listOf(
            Settings.genFood(FoodType.Worm)
        )
    )

    /* В корне находятся опавшие орехи и шишки. Их спелость - максимальная. Только орехи и шишки с
    * максимальной спелостью могут есть бурундуки */
    override val root = Root(
        listOf(
            Settings.genFood(FoodType.Nut,ripenessInTreePart = 5),
            Settings.genFood(FoodType.Cone,ripenessInTreePart = 5),
            Settings.genFood(FoodType.RootCrop)
        )
    )
}

/**
 * Клен:
 *      крона - кленовые листья
 *      ствол - червяки
 *      корни - кленовые листья и корнеплоды
 * P.s: спелость кленовых листьев не влияет на поедательную способность, можно не задавать?
 */
class Maple(coord: Pair<Int, Int> = 0 to 0) : Tree(coord) {

    override val crown = Crown(
        listOf(
            Settings.genFood(FoodType.MapleLeaf)
        )
    )

    override val trunk = Trunk(
        listOf(
            Settings.genFood(FoodType.Worm)
        )
    )

    override val root = Root(
        listOf(
            Settings.genFood(FoodType.MapleLeaf),
            Settings.genFood(FoodType.RootCrop)
        )
    )

}

/**
 * Дуб:
 *      крона - шишки
 *      ствол - червяки
 *      корни - корнеплоды и опавшие шишки
 */
class Oak(coord: Pair<Int, Int> = 0 to 0) : Tree(coord) {
    override val crown = Crown(
        listOf(
            Settings.genFood(FoodType.Cone)
        )
    )

    override val trunk = Trunk(
        listOf(
            Settings.genFood(FoodType.Worm)
        )
    )

    /* В корне находятся опавшие шишки. Их спелость - максимальная. Только шишки с
    * максимальной спелостью могут есть бурундуки */
    override val root = Root(
        listOf(
            Settings.genFood(FoodType.RootCrop),
            Settings.genFood(FoodType.Cone, ripenessInTreePart = 5)
        )
    )
}

/** Сосна
 *  Еда: крона - шишки
 *       ствол - червяки
 *       корни - шишки
 * */
class Pine(coord: Pair<Int, Int> = 0 to 0) : Tree(coord) {
    override val crown = Crown(
        listOf(
            Settings.genFood(FoodType.Cone)
        )
    )
    override val trunk = Trunk(
        listOf(
            Settings.genFood(FoodType.Worm)
        )
    )

    /* В корне находятся опавшие шишки. Их спелость - максимальная. Только шишки с
    * максимальной спелостью могут есть бурундуки */
    override val root = Root(
        listOf(
            Settings.genFood(FoodType.Cone, ripenessInTreePart = 5),
            Settings.genFood(FoodType.RootCrop)
        )
    )
}

/** Грецкий орех
 *  Еда: крона - орехи
 *       ствол - червяки
 *       корни - корнеплоды и опавшие орехи
 * */
class Walnut(coord: Pair<Int, Int> = 0 to 0) : Tree(coord) {
    override val crown = Crown(
        listOf(
            Settings.genFood(FoodType.Nut)
        )
    )
    override val trunk = Trunk(
        listOf(
            Settings.genFood(FoodType.Worm)
        )
    )

    /* В корне находятся опавшие орехи. Их спелость - максимальная. Только орехи с
    * максимальной спелостью могут есть бурундуки */
    override val root = Root(
        listOf(
            Settings.genFood(FoodType.Nut, ripenessInTreePart = 5),
            Settings.genFood(FoodType.RootCrop)
        )
    )
}
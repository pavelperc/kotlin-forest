package com.team1.forest.trees.home

import com.team1.forest.animals.Animal

/**
 * Абстрактный класс Жилое помещение
 */
abstract class Home {
    val animals = mutableListOf<Animal>()

    override fun toString(): String {
        return this.javaClass.simpleName
    }

    fun equalType(type: HomeType): Boolean{
        return type.toString() == this.javaClass.simpleName
    }
}


/** Класс Дупло*/
class Hollow : Home() {
}


/** Класс Нора*/
class Den : Home() {
}


/** Класс Гнездо*/
class Nest : Home() {
}

enum class HomeType{
    Hollow,
    Den,
    Nest
}



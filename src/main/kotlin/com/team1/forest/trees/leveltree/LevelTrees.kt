package com.team1.forest.trees.leveltree

import com.team1.forest.animals.Animal
import com.team1.forest.food.FoodType
import com.team1.forest.food.PlantFood
import com.team1.forest.settings.Settings
import com.team1.forest.trees.home.Den
import com.team1.forest.trees.home.Hollow
import com.team1.forest.trees.home.Home
import com.team1.forest.trees.home.Nest

/**
 * Абстрактный класс Уровень дерева
 */
sealed class LevelTree(
    /** Список еды поблизости */
    val food: List<PlantFood>
) {

    val currentAnimals = mutableListOf<Animal>()

    /**
     * Чтобы выжить, животному достаточно съесть какого-либо допустимого вида еды за один раз
     * Не обязательно есть и червяков и орехи*
     * Если животное не поело, то false
     */
    fun reduceResources(eatenBy: Animal): Boolean {

        /*if (eatenBy.possibleFood.contains(FoodType.Animal) && food
                .filter { food -> food.foodType == FoodType.Animal }
                .firstOrNull()?.count ?: 0 >= eatenBy.amountOfFoodToLive
        ) {
            // TODO: поедание животных должно убирать их как объекты

            //return true
        }*/

        if (eatenBy.possibleFood.contains(FoodType.Worm) && food
                .filter { food -> food.foodType == FoodType.Worm }
                .firstOrNull()?.count ?: 0 >= eatenBy.amountOfFoodToLive
        ) {
            food
                .filter { food -> food.foodType == FoodType.Worm }
                .forEach { it.reduceCount(eatenBy.amountOfFoodToLive) }

            return true
        }

        if (eatenBy.possibleFood.contains(FoodType.Cone) && food
                .filter { food -> food.foodType == FoodType.Cone }
                .firstOrNull()?.count ?: 0 >= eatenBy.amountOfFoodToLive
        ) {
            food
                .filter { food -> food.foodType == FoodType.Cone }
                .forEach { it.reduceCount(eatenBy.amountOfFoodToLive) }

            return true
        }

        if (eatenBy.possibleFood.contains(FoodType.RootCrop) && food
                .filter { food -> food.foodType == FoodType.RootCrop }
                .firstOrNull()?.count ?: 0 >= eatenBy.amountOfFoodToLive
        ) {
            food
                .filter { food -> food.foodType == FoodType.RootCrop }
                .forEach { it.reduceCount(eatenBy.amountOfFoodToLive) }

            return true
        }

        if (eatenBy.possibleFood.contains(FoodType.MapleLeaf) && food
                .filter { food -> food.foodType == FoodType.MapleLeaf }
                .firstOrNull()?.count ?: 0 >= eatenBy.amountOfFoodToLive
        ) {
            food
                .filter { food -> food.foodType == FoodType.MapleLeaf }
                .forEach { it.reduceCount(eatenBy.amountOfFoodToLive) }

            return true
        }

        if (eatenBy.possibleFood.contains(FoodType.Nut) && food
                .filter { food -> food.foodType == FoodType.Nut }
                .firstOrNull()?.count ?: 0 >= eatenBy.amountOfFoodToLive
        ) {
            food
                .filter { food -> food.foodType == FoodType.Nut }
                .forEach { it.reduceCount(eatenBy.amountOfFoodToLive) }

            return true
        }

        return false
    }

    abstract val homes: List<Home>

    override fun toString(): String {
        return "LevelTree(food=$food, currentAnimals=${currentAnimals.map { "${it.name}${it.id}" }}, homes=$homes)"
    }

    /**
     * Сопостовляет необходимую еду с имеющейся
     */
    fun checkRecources(animal: Animal): Boolean {
        // t/odo: check resources before making a decision about moving to a different tree level
        var list = currentAnimals.filter {
            it.possibleFood.any() == animal.possibleFood.any()
        }
        var countNeed: Int = 0
        var countHave: Int = 0
        for (i in 0 until list.size)
            countNeed += list[i].amountOfFoodToLive
        for(i in 0 until food.size)
            countHave = food[i].count
        return countHave >= countNeed
    }

    /**
     * Отдаёт соседей зверя
     */
    fun getNeighbors(animal: Animal): List<Animal> {
        return currentAnimals.filter { it !== animal }
    }

    /**
     * Отдаёт потенциальных возлюбленных зверя
     */
    fun getSweetheart(animal: Animal): List<Animal> {
        return currentAnimals.filter { it.javaClass == animal.javaClass && it.sex != animal.sex }
    }

}

class Crown(food: List<PlantFood> = emptyList()) : LevelTree(food) {
    override val homes = List(Settings.nestNumber) { Nest() }


}

class Trunk(food: List<PlantFood> = emptyList()) : LevelTree(food) {
    override val homes = List(Settings.hollowNumber) { Hollow() }
}

class Root(food: List<PlantFood> = emptyList()) : LevelTree(food) {
    override val homes = List(Settings.denNumber) { Den() }
}

enum class LevelEnum {
    crown,
    trunk,
    root
}
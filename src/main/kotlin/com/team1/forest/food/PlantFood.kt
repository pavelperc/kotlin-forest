package com.team1.forest.food

interface Food {
    /**
     * Показатель спелости
     */
    val ripeness: Int

    /**
     * количество
     */
    var count: Int

    /**
     * Уменьшаем count
     */
    // TODO: проверить метод на необходимость. кажется, что он - пережиток моей архитектуры
    fun eatFood(count: Int) {
        this.count -= count
    }

    /**
     * Проверка на перезрелость (для опадения сверху вниз)
     */
    fun isOverripe(): Boolean
}

/**
 * Класс Еда для вегетерианских животных
 */
class PlantFood(
    val foodType: FoodType,
    override var count: Int = 10,
    override val ripeness: Int = 1,
    var updateCount: Int = count,
    /** Max ripeness before overripe */
    private val maxRipeness: Int = 5
) : Food {

    /**
     * Проверка на перезрелость (для опадения сверху вниз)
     */
    override fun isOverripe() = ripeness > maxRipeness

    fun updateCount() {
        count += updateCount
    }

    fun reduceCount(forAmount: Int) {
        count -= forAmount
    }

    override fun toString() = "PlantFood(foodType=$foodType, count=$count)"


}

enum class FoodType {
    Cone,
    MapleLeaf,
    Nut,
    RootCrop,
    Worm
    //,    Animal
}


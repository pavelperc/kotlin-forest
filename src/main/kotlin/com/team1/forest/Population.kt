package com.team1.forest

import com.team1.forest.animals.Animal
import com.team1.forest.settings.Settings

class Population(val forest: Forest) {
    val animals: MutableList<Animal> = Settings.genAnimals(forest)

    init {
    }
}